package ch.fhnw.woipv.sdb.detectors;

import ch.fhnw.woipv.sdb.features.AbstractFeature;
import ch.fhnw.woipv.sdb.loader.AbstractVideoLoader;

/**
 * Created by Administrator on 23.03.2015.
 */
public abstract class AbstractDetector {
    protected AbstractFeature[] features;

    public AbstractDetector( AbstractFeature ... features  ){
       this.features = features;
    }

    public abstract void analyseFeatures();

    public abstract void finish();



}
