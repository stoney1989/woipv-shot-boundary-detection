package ch.fhnw.woipv.sdb.detectors;

import ch.fhnw.woipv.sdb.ShotBoundary;
import ch.fhnw.woipv.sdb.features.AbstractFeature;
import ch.fhnw.woipv.sdb.features.EdgeFeature;
import ch.fhnw.woipv.sdb.features.HistogramFeature;
import ch.fhnw.woipv.sdb.loader.AbstractVideoLoader;
import ch.fhnw.woipv.sdb.tools.XYChart;

import java.util.ArrayList;
import java.util.List;

import static ch.fhnw.woipv.sdb.features.HistogramFeature.Feature;

/**
 * Created by Administrator on 23.03.2015.
 */
public class HistogramDetector extends AbstractDetector {

    private int window;
    private ShotBoundary previousCut = new ShotBoundary(0);

    private AbstractFeature diffHistory;

    //tuning parameter for hard cut threshold
    public final double T = 9;

    //tuning parameter for gradual cut threshold
    public final double U = 2;

    //debug
    private boolean debug = false;
    private XYChart chart;
    private int[] series = new int[4];

    public HistogramDetector( AbstractVideoLoader loader, int window ){
        this(loader, window, false);
    }

    public HistogramDetector( AbstractVideoLoader loader, int window, boolean debug ){
        super( new HistogramFeature(loader) );
        //super( new EdgeFeature(loader) );
        this.debug = debug;

        this.window = window;
        diffHistory =  features[0];

        if(debug){
            chart = new XYChart("debug",(int) loader.getFrameTotal());
            series[0] = chart.createNewSeries("diff");
            series[1] = chart.createNewSeries("threshold hardcut");
            series[2] = chart.createNewSeries("hardcut");
            series[3] = chart.createNewSeries("gradualcut");
            //series[3] = chart.createNewSeries("threshold hardcut");
            chart.setZoom(1);
            chart.setupScrollBar();
            chart.pack();
            chart.setVisible(true);
        }
    }

    @Override
    public void analyseFeatures() {
        diffHistory.extractFeature();
        ShotBoundary hardCut;
        if( (hardCut = findHardCut()) != null ){
            List<ShotBoundary> gradualCut = findGradualCut( previousCut.getTo() + 1, hardCut.getFrom()  );

            previousCut = hardCut;
        }
    }

    @Override
    public void finish() {
        int from = previousCut.getTo() + 1;
        int to   = diffHistory.getCurrentIndex();
        findGradualCut( from, to  );
    }

    private ShotBoundary findHardCut(){
        ShotBoundary hardCut = null;

        int to   = diffHistory.getCurrentIndex();
        int from =  to - window;
        if( from < 0 )from = 0;
        int size = to - from;

        double max = -Double.MAX_VALUE;
        double threshold = 0;

        int dimension = HistogramFeature.Feature.HIST_DIFF.ordinal();
        int maxIndex = 0;

        if(debug)chart.addValuesToSeries(to-1,diffHistory.getData(dimension,to-1),series[0]);
        if(size < window)return null;

        for(int i = from; i < to; i++){
            double diff = diffHistory.getData(dimension,i);
            threshold += diff;
            if(diff > max) {
                max = diff;
                maxIndex = i;
            }
        }

        //threshold -= max;
        threshold /= size;
        threshold *= T;

        if(debug)chart.addValuesToSeries(to-1,threshold,series[1]);

        //TODO: flash detection
        if(max > threshold && previousCut.getTo() != maxIndex){
            //diff[i] is maximum & diff[i]>=Th_cut & diff[i] >= 2*diff[i-1] & diff[i] >= 2*diff[i+1]
//            hardCut = new ShotBoundary( maxIndex );
//            chart.addValuesToSeries(maxIndex-2,0,series[2]);
//            chart.addValuesToSeries(maxIndex-1,0,series[2]);
//            chart.addValuesToSeries(maxIndex,chart.getMaxY(),series[2]);
//            chart.addValuesToSeries(maxIndex+1,0,series[2]);
//            chart.addValuesToSeries(maxIndex+2,0,series[2]);
//
//            System.out.println(to+" "+maxIndex+" :"+max+" "+threshold );
        }

        return hardCut;
    }

    private List<ShotBoundary> findGradualCut( int from, int to ){
        double diffMean = getAverageDifference(from,to);
        List<Double> disBuffer = new ArrayList<Double>();
        double disMean  = getAverageDifferenceDistance(from,to,diffMean, disBuffer);

        double threshold = U * disMean;

        List<ShotBoundary> gradualBoundaries = markGradualCuts(from, threshold, disBuffer);

        //System.out.println(disMean);
        //chart.addValuesToSeries(from,disMean*100,series[2]);
        //chart.addValuesToSeries(to-1,disMean*100,series[2]);
        return gradualBoundaries;
    }

    //DiffMean
    private double getAverageDifference(int from, int to) {
        double result = 0;
        int size = to - from;
        for (int i = from; i < to; i++) {
            result += diffHistory.getData(Feature.HIST_DIFF.ordinal(),i);
        }
        return result / size;
    }

    //DisMean
    private double getAverageDifferenceDistance(int from, int to,double avgDiff, List<Double> disBuffer) {
        double result = 0;
        for (int i = from; i < to; i++) {
            //double dis = Math.abs(diffHistory.getData(Feature.HIST_DIFF.ordinal(), i) - avgDiff);
            double dis = diffHistory.getData(Feature.HIST_DIFF.ordinal(), i) - avgDiff;
            disBuffer.add(dis);
            result += dis;
        }
        return result / disBuffer.size();
    }

    private List<ShotBoundary> markGradualCuts(int from, double threshold, List<Double> disBuffer){
        int minK = window / 2;
        int k = 0;
        List<ShotBoundary> result = new ArrayList<ShotBoundary>();
        for (int i = 0; i < disBuffer.size(); i++) {
            double dis = disBuffer.get(i);
            //if(k>0)System.out.println((from+i)+" "+k+" "+minK);
            if( dis >= threshold  ){
                k++;
            }else{

                if( k  > minK ){

                    if(debug){
                        chart.addValuesToSeries((from+i)-k,chart.getMaxY(),series[3]);
                        chart.addValuesToSeries((from+i),chart.getMaxY(),series[3]);
                    }

                    result.add(new ShotBoundary(ShotBoundary.Type.GRADUAL,(from+i)-k,from+i));
                }else{

                    if(debug){
                        chart.addValuesToSeries((from+i)-k,0,series[3]);
                        chart.addValuesToSeries((from+i),0,series[3]);
                    }

                }
                k = 0;
            }
        }

        return result;
    }
}
