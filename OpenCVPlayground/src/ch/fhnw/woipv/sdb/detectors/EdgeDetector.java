package ch.fhnw.woipv.sdb.detectors;

import ch.fhnw.woipv.sdb.features.AbstractFeature;
import ch.fhnw.woipv.sdb.features.EdgeFeature;
import static ch.fhnw.woipv.sdb.features.EdgeFeature.Feature;
import ch.fhnw.woipv.sdb.loader.AbstractVideoLoader;
import ch.fhnw.woipv.sdb.tools.XYChart;

/**
 * Created by Administrator on 25.03.2015.
 */
public class EdgeDetector extends AbstractDetector {

    private AbstractFeature diffHistory;

    private XYChart chart;
    private int[] series = new int[4];

    public EdgeDetector(AbstractVideoLoader loader){
        super( new EdgeFeature(loader) );
        diffHistory = features[0];

        chart = new XYChart("debug",(int) loader.getFrameTotal());
        series[0] = chart.createNewSeries("ecr_out difference");
        series[1] = chart.createNewSeries("ecr_in difference");
        series[2] = chart.createNewSeries("ecr difference");

        chart.setZoom(0.25);
        chart.setupScrollBar();
        chart.pack();
        chart.setVisible(true);

    }

    @Override
    public void analyseFeatures(){
        diffHistory.extractFeature();



        int i = diffHistory.getCurrentIndex()-1;
        double diff = diffHistory.getData(Feature.ECR_IN.ordinal(),i);
        double diff2 = diffHistory.getData(Feature.ECR_OUT.ordinal(),i);

        //System.out.println(diff);
        chart.addValuesToSeries(i,diff,series[0]);
        chart.addValuesToSeries(i,diff2,series[1]);

    }

    @Override
    public void finish() {

    }
}
