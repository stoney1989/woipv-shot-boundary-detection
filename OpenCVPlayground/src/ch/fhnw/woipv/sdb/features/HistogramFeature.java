package ch.fhnw.woipv.sdb.features;

import ch.fhnw.woipv.sdb.loader.AbstractVideoLoader;
import com.atul.JavaOpenCV.Imshow;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;

/**
 * Created by Administrator on 19.03.2015.
 */
public class HistogramFeature extends AbstractFeature{

    //histogram of the previous frame
    private Mat previousHistogram;

    private MatOfFloat histRanges;
    private MatOfInt channels;
    private MatOfInt bins;

    public enum Feature{
        HIST_DIFF,
    };

    public final int BIN_SIZE = 16;

    public HistogramFeature(AbstractVideoLoader loader) {
        super(loader,Feature.values().length);
        //H: 0 - 180
        //S: 0 - 256
        histRanges = new MatOfFloat(0,180, 0,256);
        channels = new MatOfInt(0,1);
        bins = new MatOfInt(BIN_SIZE,BIN_SIZE);
    }


    Imshow im = new Imshow("Show");
    @Override
    public void extract() {
        Mat frame = loader.getFrame();

        //get histogram
        Mat histogram = getHistogram(frame);

        //clean up
        frame.release();

        double diff = 0;
        if( previousHistogram != null ){
            diff = calcHistogramDifference(histogram, previousHistogram);
            previousHistogram.release();
        }
        previousHistogram = histogram;
        setData(Feature.HIST_DIFF.ordinal(),getCurrentIndex(),diff);
    }

    private static double calcHistogramDifference(Mat histogramA, Mat histogramB) {
        double sum = 0;
        double difference = 0;
        for (int j = 0; j < histogramA.rows(); j++) {
            for (int i = 0; i < histogramA.cols(); i++) {
                difference = Math.abs(histogramA.get(j, i)[0] - histogramB.get(j, i)[0]);
                sum += difference * difference;
            }
        }
        return Math.sqrt(sum)/(histogramA.rows()*histogramA.cols());
    }

    private Mat getHistogram(Mat frame) {
        Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2HSV);
        Mat histogram = new Mat(), mask = new Mat();
        Imgproc.calcHist(Arrays.asList(frame), channels, mask, histogram, bins, histRanges);
        mask.release();
        //???
        //Core.normalize(histogram, histogram);
        return histogram;
    }
}
