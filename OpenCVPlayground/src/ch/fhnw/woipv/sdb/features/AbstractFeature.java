package ch.fhnw.woipv.sdb.features;

import ch.fhnw.woipv.sdb.loader.AbstractVideoLoader;

/**
 * Created by Administrator on 19.03.2015.
 */
public abstract class AbstractFeature {

    private double[][] data;
    protected AbstractVideoLoader loader;

    private int currentIndex = 0;

    public AbstractFeature(AbstractVideoLoader loader, int dimensions){
        this.loader = loader;
//        data = new double[ dimensions ][ loader.getFrameTotal()  ];
        data = new double[ dimensions ][ loader.getFrameTotal() ];
    }

    public int getDimensions(){
        return this.data.length;
    }

    public void extractFeature(){
        extract();
        currentIndex++;
    }

    protected abstract void extract();

    public int getCurrentIndex() {
        return currentIndex;
    }

    public double getData( int dimension,int index ) {

        return data[dimension][index];
    }

    public void setData( int dimension,int index, double value ) {
        data[dimension][index] = value;
    }
}
