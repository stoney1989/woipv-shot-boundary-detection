package ch.fhnw.woipv.sdb.features;

import ch.fhnw.woipv.sdb.loader.AbstractVideoLoader;
import ch.fhnw.woipv.sdb.loader.BufferedVideoLoader;
import com.atul.JavaOpenCV.Imshow;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Administrator on 20.03.2015.
 */
public class EdgeFeature extends AbstractFeature {
    public EdgeFeature(AbstractVideoLoader loader) {
        super(loader,Feature.values().length);
    }

    private Mat previousEdgeFrame;
    private Mat previousInvertFrame;
    //s_n-1
    private int previousEdgePixels = 0;

    public final int DILATION_SIZE = 20;

    public enum Feature{
        ECR,
        ECR_IN,
        ECR_OUT
    };

    //canny detection parameters
    public final int CANNY_LOWER_THRESHOLD = 20;
    public final int CANNY_RATIO = 3;
    public final int CANNY_UPPER_THRESHOLD = CANNY_LOWER_THRESHOLD * CANNY_RATIO;

    Imshow im = new Imshow("Show");


    @Override
    public void extract() {

        double ecr_in  = 0;
        double ecr_out = 0;
        double ecr = 0;

        //current frame
        Mat frame = loader.getFrame(0);

        int rows = frame.rows();
        int cols = frame.cols();
        int type = frame.type();
        Mat grayFrame   = new Mat( rows, cols, type );
        Mat edgeFrame   = new Mat( rows, cols, type );
        Mat invertFrame = new Mat( rows, cols, type );

        //transform to grayscale
        Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
        frame.release();

        //use canny edge detection:
        //reduce noise
        Imgproc.blur(grayFrame, grayFrame, new Size(3, 3));
        Imgproc.Canny(grayFrame, edgeFrame, CANNY_LOWER_THRESHOLD, CANNY_UPPER_THRESHOLD, 3, true);
        grayFrame.release();

        //im.showImage(edgeFrame);

        //count edge pixels (s_n)
        int edgePixels = Core.countNonZero(edgeFrame);



        //dilate + invert edge frame
        Imgproc.dilate(edgeFrame, invertFrame, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(DILATION_SIZE,DILATION_SIZE)));
        Core.bitwise_not(invertFrame, invertFrame);

        im.showImage(invertFrame);

        if( previousEdgeFrame != null && previousInvertFrame != null){
            Mat ecFrame = new Mat( rows, cols, type );

            Core.bitwise_and(previousInvertFrame,edgeFrame,ecFrame);
            double ec_in = Core.countNonZero(ecFrame);

            //im.showImage(ecFrame);

            Core.bitwise_and(invertFrame, previousEdgeFrame,ecFrame);
            double ec_out = Core.countNonZero(ecFrame);

            //calculate Edge Change Ratio
            ecr_in  = ec_in  / edgePixels;
            ecr_out = ec_out / previousEdgePixels;

            ecr = Math.max(ecr_in, ecr_out);

//            if(ecr>0.3)System.out.println(ecr);

            //clean up
            previousEdgeFrame.release();
            previousInvertFrame.release();
            ecFrame.release();
        }
        previousEdgeFrame   = edgeFrame;
        previousInvertFrame = invertFrame;
        previousEdgePixels  = edgePixels;

        try{
            setData(Feature.ECR_IN.ordinal(),getCurrentIndex(),ecr_in);
            setData(Feature.ECR_OUT.ordinal(),getCurrentIndex(),ecr_out);
            setData(Feature.ECR.ordinal(),getCurrentIndex(),ecr);
        }catch(Exception e){

        }


    }
}
