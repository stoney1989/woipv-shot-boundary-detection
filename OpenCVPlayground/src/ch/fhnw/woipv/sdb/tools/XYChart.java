package ch.fhnw.woipv.sdb.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

public class XYChart extends ApplicationFrame{

	private XYDataset dataset;
	private JFreeChart chart;
	private ChartPanel chartPanel;
	private List<XYSeries> series = new ArrayList<XYSeries>();
	
	private int width;
	private int height;
	private int totalFrames;
	
	private int range;
	private double zoom = 1;

    private double maxY = 0.01;
	
	private JScrollBar hbar;
	
	
	public void setupScrollBar( ){
		range = (int) (width * zoom);
		hbar.setMaximum(totalFrames - range);
		hbar.setMinimum(0);
		//hbar.setValue(0);		
	}
	
	public void setZoom(double zoom){
		this.zoom = zoom;
	}
	
	public double getZoom(){
		return this.zoom;
	}
	
	public XYChart(String title, int totalFrames) {
		super(title);
		this.totalFrames = totalFrames;
		dataset = new XYSeriesCollection();
		chart = createChart(dataset);
		chartPanel = new ChartPanel(chart);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        width = (int) screenSize.getWidth()-100;
        height = (int) screenSize.getHeight()-100;
		chartPanel.setPreferredSize(new java.awt.Dimension(width, height));
		JPanel content = new JPanel(new BorderLayout());
        content.add(chartPanel);
        
        
        //System.out.println(width+" "+height);
        //setContentPane(content);
        
        add(content,BorderLayout.NORTH);
        
        hbar = new JScrollBar(JScrollBar.HORIZONTAL, 0, 1, 0, 8000);
        hbar.addAdjustmentListener(new MyAdjustmentListener());
        
        add(hbar, BorderLayout.CENTER);
        
        JButton zoomIn = new JButton("+");
        zoomIn.addActionListener(new MyButtonListener1());
        
        add(zoomIn,BorderLayout.WEST);
        
        JButton zoomOut = new JButton("-");
        zoomOut.addActionListener(new MyButtonListener2());
        
        add(zoomOut,BorderLayout.EAST);
        
        setupScrollBar();
        
//		JScrollPane sp = new JScrollPane(chartPanel);
//        setContentPane(sp);
//        setSize(400, 200);
	}
	
	public void setXAxisRange(double from, double to){
		chart.getXYPlot().getDomainAxis().setRange(from,to);
		this.pack();
		
	};
	
    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createXYLineChart(
            "SBD - Shot Boundary Detection", 
            "Frame", 
            "Value",
            dataset, 
            PlotOrientation.VERTICAL,
            true, 
            true, 
            false
        );
        final XYPlot plot = result.getXYPlot();
       
        plot.getDomainAxis().setRange(0,1000);
        plot.getRangeAxis().setRange(0,maxY);
        return result;
    }
    
    public int createNewSeries(String title){
    	this.series.add(new XYSeries(title));
    	((XYSeriesCollection)this.dataset).addSeries(series.get(series.size()-1));
    	return this.series.size()-1;
    } 
    
    public void addValuesToSeries(double x, double y, int seriesIndex){
        if(y>maxY){
            maxY = y;
            chart.getXYPlot().getRangeAxis().setRange(0,maxY);
        }
    	this.series.get(seriesIndex).add(x, y);
    	this.pack();
    }

    public double getMaxY() {
        return maxY;
    }

    class MyAdjustmentListener implements AdjustmentListener {
        public void adjustmentValueChanged(AdjustmentEvent e) {
        	setXAxisRange(hbar.getValue(),hbar.getValue()+range);
        	repaint();
        }
    }
    
    class MyButtonListener1 implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			setZoom(getZoom()/2);
			setupScrollBar();
			repaint();
		}
    	
    }
    
    class MyButtonListener2 implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			setZoom(getZoom()*2);
			setupScrollBar();
			repaint();
		}
    	
    }

}
