package ch.fhnw.woipv.sbd.Detectors;

/**
 * Created by Nico Hartmann on 05.05.2015.
 */
public interface IWindowFunction {
    void executeFunction( int from, int to, int middle, double[] window );
    void executeWindow();
}
