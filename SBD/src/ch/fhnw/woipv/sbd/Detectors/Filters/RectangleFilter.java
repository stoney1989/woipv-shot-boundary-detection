package ch.fhnw.woipv.sbd.Detectors.Filters;

/**
 * Created by Nico Hartmann on 24.04.2015.
 */
public class RectangleFilter implements I2DFilter {

    private final int[][] rectangleFilter;
    private final int window;

    private static final int WEIGHT    = 0;
    private static final int INDEX_MOD = 1;

    public RectangleFilter(int window){
        this.window = window;
        rectangleFilter = new int[2][window];//{ {1,1,1,1,1,1,1}, { -3, -2, -1, 0, 1, 2, 3 } };
        int m = window / 2;
        for(int i = 0; i < window; i++){
            rectangleFilter[WEIGHT][i] = 1;
            rectangleFilter[INDEX_MOD][i] = i - m;
        }
    }

    @Override
    public  double[] useFilter(double[] data) {
        double[] result = new double[data.length];
        for(int i = 0; i < data.length; i++){
            double sum = 0.0;
            int count = 0;
            for(int j = 0; j < window; j++){
                int index = i + rectangleFilter[INDEX_MOD][j];
                if( index >= 0 && index < data.length ){
                    sum += data[index] * rectangleFilter[WEIGHT][j];
                    count++;
                }
            }
            if( count > 0 && sum > 0 ){
                sum /= count;
                result[i] = sum;
            }
        }
        return result;
    }
}
