package ch.fhnw.woipv.sbd.Detectors.Filters;

/**
 * Created by Nico Hartmann on 24.04.2015.
 */
public interface I2DFilter {
    double[] useFilter( double[] data );
}
