package ch.fhnw.woipv.sbd.Detectors;

import ch.fhnw.woipv.sbd.Detectors.Filters.I2DFilter;
import ch.fhnw.woipv.sbd.Detectors.Filters.RectangleFilter;
import ch.fhnw.woipv.sbd.Visualisation.XYChart;
import org.jfree.chart.ChartColor;

import java.awt.*;

/**
 * Created by Nico Hartmann on 05.05.2015.
 */
public class BasicDetector extends AbstractDetector {

    private final int windowSize;
    private final XYChart chart;

    public BasicDetector(  int windowSize, XYChart chart, double[] ... data ){
        super(data);
        this.windowSize = windowSize;
        System.out.println(windowSize);
        this.chart = chart;
    }

    @Override
    public void run() {




        //double[][] fData = new double[data.length][];

        for( int i = 0; i < this.data.length; i++ ){
            double[] dataSeries = data[i];
            //double[] filteredDataSeries = rectFilter.useFilter( dataSeries );

            drawSeries(dataSeries,chart,0);
            final double globalAvg = getAverage(dataSeries);
            chart.addHorizontalLine(globalAvg*4, Color.BLUE);


            new WindowFunction(0,dataSeries.length-1,windowSize,dataSeries) {
                final int LOCAL_THRESHOLD_MOD  = 4;
                final int GLOBAL_THRESHOLD_MOD = 4;
                @Override
                public void executeFunction(int from, int to, int m, double[] window) {
                    double localAvg = 0;
                    boolean isMaximum = true;
                    for (int j = from; j <= to && isMaximum; j++) {
                        if(window[j] > window[m])isMaximum = false;
                        localAvg += window[j];
                    }
                    localAvg /= to - from;



                    chart.addValuesToSeries(m,localAvg * LOCAL_THRESHOLD_MOD,1);

                    if(isMaximum && window[m] > localAvg * LOCAL_THRESHOLD_MOD && window[m] > globalAvg * GLOBAL_THRESHOLD_MOD){
                        //chart.addVerticalLine(m, ChartColor.VERY_DARK_CYAN);
                        cuts.add(new ShotBoundary(m));
                    }

                }
            }.executeWindow();


            final double[] segmentAverage = new double[dataSeries.length];

            ShotBoundary previousCut = null;

            //flaten data
            for( ShotBoundary c : cuts ){
                dataSeries[ c.getFrom() ] = 0.0;
                int from = 0;
                if( previousCut != null){
                    from = previousCut.getTo();
                }
                int to = c.getFrom();
                java.util.Arrays.fill(segmentAverage,from,to,getMean(from, to, dataSeries));
                previousCut = c;
            }
            if(previousCut == null){
                java.util.Arrays.fill(segmentAverage,0,dataSeries.length-1,getMean(0,dataSeries.length-1,dataSeries));
            }else{
                java.util.Arrays.fill(segmentAverage,previousCut.getTo(),dataSeries.length-1,getMean(0, dataSeries.length - 1, dataSeries));
            }

            I2DFilter rectFilter = new RectangleFilter( windowSize / 8    );
            I2DFilter rectFilter2 = new RectangleFilter( windowSize   );
            dataSeries = rectFilter.useFilter(rectFilter.useFilter(rectFilter.useFilter(rectFilter.useFilter(dataSeries))));

            final double newGlobalAvg = getMean(dataSeries);


            new WindowFunction(0,dataSeries.length-1,windowSize *2 ,dataSeries) {

                int k = 0;
                //int kt = (int)(windowSize*0.9);
                int kt = (int)(windowSize/2);

                @Override
                public void executeFunction(int from, int to, int middle, double[] window) {
                    double mean = getMean(from, to, window);

                    double t = (segmentAverage[middle]+globalAvg+mean)/3;
                    if(window[middle] > t){
                        k++;
                    }else{
                        if(k > kt ){
                            cuts.add(new ShotBoundary(ShotBoundary.Type.GRADUAL, middle-k-1, middle -1));
                            //chart.addVerticalLine(middle-1, ChartColor.DARK_RED);
                            //chart.addVerticalLine(middle-k-1, ChartColor.DARK_RED);
                        }
                        k=0;
                    }


                    chart.addValuesToSeries(middle,window[middle],2);

                    chart.addValuesToSeries(middle,(segmentAverage[middle]+globalAvg+mean)/3,3);

                }
            }.executeWindow();

        }

        //double[] data = new RectangleFilter(loader.getFramesPerSecond()).useFilter(historyFeature.getData()[0]);
    }
}
