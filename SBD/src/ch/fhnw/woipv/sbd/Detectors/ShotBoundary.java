package ch.fhnw.woipv.sbd.Detectors;

/**
 * Created by Nico Hartmann on 24.04.2015.
 */
public class ShotBoundary {
    public Type getType() {
        return type;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public enum Type{
        CUT,
        GRADUAL
    }

    private final Type type;
    private int from;
    private int to;

    public ShotBoundary( int hardCutIndex ){
        this(Type.CUT,hardCutIndex,hardCutIndex);
    }

    public ShotBoundary( int from, int to ){
        this(Type.GRADUAL, from, to);
    }

    public ShotBoundary( Type type, int from, int to ){
        this.type = type;
        this.from = from;
        this.to = to;
    }
}
