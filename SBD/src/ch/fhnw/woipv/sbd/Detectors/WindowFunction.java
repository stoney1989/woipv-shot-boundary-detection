package ch.fhnw.woipv.sbd.Detectors;

/**
 * Created by Nico Hartmann on 05.05.2015.
 */
public abstract class WindowFunction implements IWindowFunction{


    private int from;
    private int to;
    private final int windowSize;
    private final double[] values;

    public WindowFunction(int from, int to, int windowSize, double[] values){
        this.from = (from < 0)? 0 : from;
        this.to = (to >= values.length)? values.length - 1 : to;
        this.windowSize = windowSize / 2;
        this.values = values;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    @Override
    public void executeWindow(){
        int m = from;
        while( m <= to ){
            int innerFrom = m - windowSize;
            int innerTo   = m + windowSize;
            if(innerFrom < from) innerFrom = from;
            if(innerTo   > to  ) innerTo   = to;
            executeFunction(innerFrom, innerTo, m, values);
            m++;
        }
    }
}
