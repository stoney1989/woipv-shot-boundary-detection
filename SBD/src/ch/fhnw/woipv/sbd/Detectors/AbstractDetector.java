package ch.fhnw.woipv.sbd.Detectors;

import ch.fhnw.woipv.sbd.Visualisation.XYChart;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Nico Hartmann on 24.04.2015.
 */
public abstract class AbstractDetector implements Runnable {

    protected final double data[][];
    protected final List<ShotBoundary> cuts;


    public static double getAverage( int from, int to, double[] values ){
        double avg = 0.0;
        for(int i = from; i <= to; i++){
            avg += values[i]*values[i];
        }
        return Math.sqrt(avg / (to-from) );
    }

    public static double getAverage(double[] values ){
        return getAverage(0,values.length-1,values);
    }

    public static double getMean(int from, int to, double[] values ){
        double diffMean = 0.0;
          for (int j = from; j <= to; j++) {
                diffMean += values[j];
          }
          diffMean /= (to - from);
        return diffMean;
    }

    public static double getMean(double[] values ){
        return getMean(0, values.length-1,values);
    }


    public static double getAverageDistance(int from, int to, double value, double[] values ){
        double disMean = 0.0;
        for (int j = from; j <= to; j++) {
            disMean += values[j] - value ;
        }
        disMean /= (to - from);
        return disMean;
    }

    public static void drawSeries(double[] values, XYChart chart, int series ){
        drawSeries(0,values.length - 1, values, chart, series);
    }

    public static void drawSeries( int from, int to, double[] values, XYChart chart, int series ){
        for(int i = from; i <= to; i++){
            chart.addValuesToSeries(i,values[i],series);
        }
    }

    public AbstractDetector( double[] ... dataBlocks ){
        this.data = new double[dataBlocks.length][];
        for( int i = 0; i < this.data.length; i++ ){
            this.data[i] = dataBlocks[i].clone();
        }
        cuts = new ArrayList<ShotBoundary>();
    }

    public List<ShotBoundary> getCuts(){
        return cuts;
    }
}
