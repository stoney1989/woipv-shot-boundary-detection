package ch.fhnw.woipv.sbd;

import ch.fhnw.woipv.sbd.Detectors.AbstractDetector;
import ch.fhnw.woipv.sbd.Detectors.BasicDetector;
import ch.fhnw.woipv.sbd.Detectors.ShotBoundary;
import ch.fhnw.woipv.sbd.Loader.VideoLoader;
import ch.fhnw.woipv.sbd.Features.*;
import ch.fhnw.woipv.sbd.Visualisation.XYChart;
import com.atul.JavaOpenCV.Imshow;

import org.jfree.chart.ChartColor;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 23.04.2015.
 */
public class ShotBoundaryDetector {

    static{
        System.loadLibrary("opencv_java2411");
    }

    public static final int CHUNK_SIZE = 32;


    public ShotBoundaryDetector(String className, String file){

        final VideoLoader loader = new VideoLoader("./videos/"+file);
        AbstractFeature feature = null;

        ClassLoader classLoader = this.getClass().getClassLoader();
        try {
            Class<?> c = Class.forName("ch.fhnw.woipv.sbd.Features."+className);
            Constructor constructor = c.getConstructor(Integer.class);
            feature = (AbstractFeature)constructor.newInstance(new Integer(loader.getVideoSize()));
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (InvocationTargetException e1) {
            e1.printStackTrace();
        } catch (NoSuchMethodException e1) {
            e1.printStackTrace();
        } catch (InstantiationException e1) {
            e1.printStackTrace();
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }

        Mat previousLastFrame = null;

        if(feature != null){
            Mat[] thumbnailBuffer =  new Mat[loader.getVideoSize()];
            int thumbnailIndex = 0;
            //Imshow im = new Imshow("preview");

            boolean finished = false;
            while(!finished){
                Mat[] frameBuffer = new Mat[CHUNK_SIZE];
                int currentIndex = loader.getCurrentPosition() + 1;
                for( int i = 0; i < CHUNK_SIZE; i++){
                    try {
                        frameBuffer[i] = loader.windToNextFrame().extractFrameMatrix();
                        thumbnailBuffer[thumbnailIndex] = new Mat();
                        Imgproc.resize(frameBuffer[i],thumbnailBuffer[thumbnailIndex], new Size(300,200));
                        //im.showImage(thumbnailBuffer[thumbnailIndex]);
                        thumbnailIndex++;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IndexOutOfBoundsException e){
                        finished = true;
                        break;
                    }
                }

                VideoChunk mainChunk = new VideoChunk( frameBuffer, currentIndex, 2 );
                Mat lastFrame = mainChunk.getFrame( mainChunk.getSize() - 1 );

                //calculate frame features
               feature.feedChunk(mainChunk);


                //calculate in-between frame features
                if( previousLastFrame != null ){
                    VideoChunk subChunk = new VideoChunk( new Mat[]{ previousLastFrame, mainChunk.getFrame(0)}, currentIndex - 1, 2 );
                    feature.feedChunk(subChunk);
                }
                previousLastFrame = lastFrame;
            }

            AbstractFeature.awaitShutDown();

            System.out.println("finish");


            XYChart chart = new XYChart(className+" - Analyse", thumbnailBuffer );

            int serie  = chart.createNewSeries("Feature", ChartColor.ORANGE);
            int serie2 = chart.createNewSeries("Abrupt Local Threshold", ChartColor.VERY_LIGHT_BLUE);
            int debug2 = chart.createNewSeries("Filtered Feature",ChartColor.RED);
            int debug3 = chart.createNewSeries("Gradual Local Threshold",ChartColor.DARK_RED);
            chart.setZoom(1);
            chart.setupScrollBar();
            chart.pack();
            chart.setVisible(true);

            AbstractDetector detector = new BasicDetector(loader.getFramesPerSecond(), chart,feature.getData()[0]);
            detector.run();

            List<ShotBoundary> gradualCuts = detector.getCuts();
            List<ShotBoundary> hardCuts = new ArrayList<ShotBoundary>();

            for(int j = gradualCuts.size() - 1; j >= 0; j--){
                if( gradualCuts.get(j).getType() == ShotBoundary.Type.CUT ) hardCuts.add(gradualCuts.remove(j) );
            }

            boolean[] remove = new boolean[ gradualCuts.size() ];
            for(int i = 0; i < gradualCuts.size(); i++){
                if( !remove[i] ){
                    ShotBoundary cut = gradualCuts.get(i);
                    for(int j = 0; j < gradualCuts.size(); j++){
                        if( i != j && !remove[j] ){
                            ShotBoundary otherCut = gradualCuts.get(j);
                            if( (cut.getFrom() >= otherCut.getFrom() && cut.getFrom() <= otherCut.getTo()) ||
                                (cut.getTo()   >= otherCut.getFrom() && cut.getTo()   <= otherCut.getTo())   ){
                                remove[j] = true;
                                int from = Math.min(cut.getFrom(), otherCut.getFrom());
                                int to   = Math.max(cut.getTo(), otherCut.getTo());
                                gradualCuts.set(i, new ShotBoundary(from,to));
                            }
                        }
                    }
                }
            }

            for(ShotBoundary hardCut : hardCuts)chart.addVerticalLine(hardCut.getFrom(), ChartColor.VERY_DARK_CYAN);
            for(int i = 0; i < gradualCuts.size(); i++){
                if( !remove[i] ){
                    chart.addVerticalLine( gradualCuts.get(i).getFrom(), ChartColor.DARK_RED);
                    chart.addVerticalLine( gradualCuts.get(i).getTo(), ChartColor.DARK_RED);
                }
            }

        }


    }


    public static void main( String args[] ){

        if (args.length < 1) throw new IllegalArgumentException("Missing path argument!");
        String path = args[0];

        final VideoLoader loader = new VideoLoader(path);
        Mat[] thumbnailBuffer =  new Mat[loader.getVideoSize()];
        int thumbnailIndex = 0;

        AbstractFeature historyFeature = new HistogramFeature(loader.getVideoSize());
        AbstractFeature edgeFeature    = new EdgeFeature(loader.getVideoSize());


        Mat previousLastFrame = null;

        Imshow im = new Imshow("preview");

        boolean finished = false;
        while(!finished){
            Mat[] frameBuffer = new Mat[CHUNK_SIZE];
            int currentIndex = loader.getCurrentPosition() + 1;
            for( int i = 0; i < CHUNK_SIZE; i++){
                try {
                    frameBuffer[i] = loader.windToNextFrame().extractFrameMatrix();

                    thumbnailBuffer[thumbnailIndex] = new Mat();
                    Imgproc.resize(frameBuffer[i],thumbnailBuffer[thumbnailIndex], new Size(300,200));
                    im.showImage(thumbnailBuffer[thumbnailIndex]);
                    thumbnailIndex++;

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IndexOutOfBoundsException e){
                    finished = true;
                    break;
                }
            }

            VideoChunk mainChunk = new VideoChunk( frameBuffer, currentIndex, 2 );
            Mat lastFrame = mainChunk.getFrame( mainChunk.getSize() - 1 );

            //calculate frame features
            historyFeature.feedChunk(mainChunk);
            edgeFeature.feedChunk(mainChunk);

            //calculate in-between frame features
            if( previousLastFrame != null ){
                VideoChunk subChunk = new VideoChunk( new Mat[]{ previousLastFrame, mainChunk.getFrame(0)}, currentIndex - 1, 2 );
                historyFeature.feedChunk(subChunk);
                edgeFeature.feedChunk(subChunk);
            }
            previousLastFrame = lastFrame;
        }

        AbstractFeature.awaitShutDown();

        System.out.println("finish");


        XYChart chart = new XYChart("Analyse", thumbnailBuffer );
        int serie  = chart.createNewSeries("Feature", ChartColor.ORANGE);
        int serie2 = chart.createNewSeries("Abrupt Local Threshold", ChartColor.VERY_LIGHT_BLUE);
        int debug2 = chart.createNewSeries("Filtered Feature",ChartColor.DARK_RED);
        int debug3 = chart.createNewSeries("Gradual Local Threshold",ChartColor.DARK_RED);
        chart.setZoom(1);
        chart.setupScrollBar();
        chart.pack();
        chart.setVisible(true);

        AbstractDetector detector = new BasicDetector(loader.getFramesPerSecond(), chart,edgeFeature.getData()[0]);
        detector.run();

//
//      double[] data = new RectangleFilter(loader.getFramesPerSecond()).useFilter(historyFeature.getData()[0]);
//
//        double globalAvg = 0.0;
//        for(int i = 0; i < data.length; i++){//
//            //chart.addValuesToSeries(i,edgeFeature.getData()[0][i],serie);
//            //if(data[i] <= historyFeature.getData()[0][i])chart.addValuesToSeries(i,historyFeature.getData()[0][i],serie);
//            chart.addValuesToSeries(i,edgeFeature.getData()[0][i],serie);
//            chart.addValuesToSeries(i,data[i],serie2);
//            globalAvg += data[i]*data[i];
//        }
//
//        globalAvg /= data.length;
//        globalAvg = Math.sqrt(globalAvg);
//        chart.addHorizontalLine(globalAvg, Color.BLUE);
//
//
//
//        int i = 0;
//        int w = loader.getFramesPerSecond();
//        while( i < data.length){
//            int from = i - w;
//            int to = i + w;
//
//            if(from < 0) from = 0;
//            if(to >= data.length)to = data.length -1;
//
//
//
//            //find local maximum
//            double avg = 0;
//            boolean isMaximum = true;
//            for (int j = from; j <= to && isMaximum; j++) {
//                if(data[j] > data[i])isMaximum = false;
//                avg += data[j];
//            }
//
//            if(isMaximum){
//                avg /= to - from;
//                if(data[i] > avg * 9 && data[i] > globalAvg * 4)chart.addVerticalLine(i, Color.GREEN);
//            }
//            //-------------------
//
//            double diffMean = 0;
//            for (int j = from; j <= to; j++) {
//                diffMean += data[j];
//            }
//            diffMean /= (to - from);
//
//            double disMean = 0;
//            for (int j = from; j <= to; j++) {
//                disMean += Math.abs(data[j]-diffMean);
//            }
//            disMean /= (to - from);
//
//            chart.addValuesToSeries(i,diffMean,debug);
//            chart.addValuesToSeries(i,disMean*1,debug2);
//            chart.addValuesToSeries(i,Math.abs(diffMean-data[i]),debug3);
//
//            int k = w / 4 ;
//            int kiR = i;
//            int kiL = i;
//            while( kiR < data.length && data[kiR] <= data[i]  &&  data[kiR] >= diffMean && data[kiR] >= globalAvg )kiR++;
//            while( kiL >= 0          && data[kiL] <= data[i]  &&  data[kiL] >= diffMean && data[kiL] >= globalAvg )kiL--;
//            //int ki = Math.min((i-kiL),(kiR-i))*2;
//            int ki = ((i-kiL)+(kiR-i))/2;
//            if( ki > k){
//                System.out.println(i+" "+(i-kiL)+" "+(kiR-i)+" "+ki);chart.addVerticalLine(i, Color.MAGENTA);
//            }

//            for (int j = from; j <= to; j++) {
//                if( data[j] >= diffMean && data[j] >= globalAvg){
//
//                    ki++;
//                }else{
//
//                    if(ki >= k){
//                        //System.out.println(ki);
//                        chart.addVerticalLine(j, Color.MAGENTA);
//                        //chart.addVerticalLine(j-ki, Color.MAGENTA);
//                    }
//                    ki = 0;
//                }
//            }
//
//
//            i++;
//        }







//        for(FutureTask<List<AbstractPartition>> task : partitionTasks){
//            try {
//                List<AbstractPartition> partitions = task.get();
//                for(AbstractPartition partition : partitions){
//                    if( !featureMap.containsKey(partition.extractorType()) )featureMap.put(partition.extractorType(), new double[ partition.getDimensionSize() ][ loader.getVideoSize() ] );
//
//                    for(int j = 0; j < partition.getData().length; j++ ){
//                        if( partition.isSubPartition() ){
//                            if( partition.getData()[j].length == 2 ){
//                                featureMap.get(partition.extractorType())[j][partition.to()] = partition.getData()[j][1];
//                            }
//                        }else{
//                            int offset   = 1;
//                            int length   = partition.getData()[j].length-offset;
//                            int position = partition.from()+offset;
//                            if(length > 0) System.arraycopy(partition.getData()[j],offset,featureMap.get(partition.extractorType())[j],position,length);
//
////                            for( int i = 0; i < a.getData()[j].length; i++ ){
////                                featureMap.get(a.extractorType())[j][i+a.from()] = a.getData()[j][i];
////                            }
//                        }
//                    }
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//        }
//
//        im.setCloseOption(1);
//        im.Window.dispose();
//        //im.Window.setVisible(false);
//
//        XYChart chart = new XYChart("debug", thumbnailBuffer );
//        int serie = chart.createNewSeries("diff");
//        int serie2 = chart.createNewSeries("diff2");
//        chart.setZoom(1);
//        chart.setupScrollBar();
//        chart.pack();
//        chart.setVisible(true);
//
//        String test = "history";
//
//        //featureMap.get(test)[0] = SimpleEdgeFilter.filter(featureMap.get(test)[0]);
//        //featureMap.get(test)[0] = LowPassFilter.filter(featureMap.get(test)[0], 1);
//
//        double data[] = featureMap.get(test)[0];
//
//
//        double globalAvg = 0.0;
//        for( int j = 0; j < data.length; j++){
//            chart.addValuesToSeries(j,data[j],serie);
//            //if(featureMap.get(test).length > 1)chart.addValuesToSeries(j,featureMap.get(test)[2][j],serie2);
//            globalAvg += data[j];
//        }
//        globalAvg /= data.length;
//        chart.addHorizontalLine(globalAvg*2, Color.BLUE);
//
//        int i = 0;
//        int w = 20;
//        while( i < data.length){
//            int from = i - w;
//            int to = i + w;
//
//            if(from < 0) from = 0;
//            if(to >= data.length)to = data.length -1;
//
//
//            double avg = 0;
//            boolean isMaximum = true;
//            for (int j = from; j <= to && isMaximum; j++) {
//                if(data[j] > data[i])isMaximum = false;
//                avg += data[j];
//            }
//
//            if(isMaximum){
//                avg /= to - from;
//                if(data[i] > avg * 9 && data[i] > globalAvg * 2)chart.addVerticalLine(i, Color.GREEN);
//            }
//            i++;
//        }

        //featureMap.get(test)[0] = LowPassFilter.filter(featureMap.get(test)[0], 20);
//        featureMap.get(test)[0] = LowPassFilter.filter(featureMap.get(test)[0], 20);
//        featureMap.get(test)[0] = LowPassFilter.filter(featureMap.get(test)[0], 20);

        //featureMap.get(test)[0] = SimpleEdgeFilter.filter(featureMap.get(test)[0]);



        //chart.addHorizontalLine(avg, Color.GREEN);
        //chart.addHorizontalLine(dis, Color.BLUE);








    }




}
