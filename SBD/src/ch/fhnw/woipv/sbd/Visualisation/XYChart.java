package ch.fhnw.woipv.sbd.Visualisation;

import ch.fhnw.woipv.sbd.Loader.VideoLoader;
import com.atul.JavaOpenCV.Imshow;
import org.jfree.chart.*;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleEdge;
import org.opencv.core.Mat;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XYChart extends ApplicationFrame{

	private XYDataset dataset;
	private JFreeChart chart;
	private ChartPanel chartPanel;
	private List<XYSeries> series = new ArrayList<XYSeries>();
	
	private int width;
	private int height;
	private int totalFrames;
	
	private int range;
	private double zoom = 1;

    private double maxY = 0.01;
	
	private JScrollBar hbar;
	
	
	public void setupScrollBar( ){
		range = (int) (width * zoom);
		hbar.setMaximum(totalFrames - range);
		hbar.setMinimum(0);
		//hbar.setValue(0);		
	}
	
	public void setZoom(double zoom){
		this.zoom = zoom;
	}
	
	public double getZoom(){
		return this.zoom;
	}

    private Mat[] buffer;
    private Imshow im;
	
	public XYChart(String title, final Mat[] buffer) {
		super(title);
        im = new Imshow(title);
        this.buffer = buffer;
		this.totalFrames = buffer.length;
		dataset = new XYSeriesCollection();
		chart = createChart(dataset);
		chartPanel = new ChartPanel(chart);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        width = (int) screenSize.getWidth()-100;
        height = (int) screenSize.getHeight()-100;
		chartPanel.setPreferredSize(new Dimension(width, height));
		JPanel content = new JPanel(new BorderLayout());
        content.add(chartPanel);



        chartPanel.addChartMouseListener(new ChartMouseListener() {

            private void show(ChartMouseEvent event){
                Rectangle2D dataArea = chartPanel.getScreenDataArea();
                JFreeChart chart = event.getChart();
                XYPlot plot = (XYPlot) chart.getPlot();
                ValueAxis xAxis = plot.getDomainAxis();
                int x = (int)xAxis.java2DToValue(event.getTrigger().getX(), dataArea,
                        RectangleEdge.BOTTOM);


                    if(x < buffer.length && x >= 0 && buffer[x] != null){

                        im.showImage(buffer[x]);

                    }

            }

            @Override
            public void chartMouseClicked(ChartMouseEvent event) {

            }

            @Override
            public void chartMouseMoved(ChartMouseEvent event) {
                show(event);
            }
        });
        
        //System.out.println(width+" "+height);
        //setContentPane(content);
        
        add(content, BorderLayout.NORTH);
        
        hbar = new JScrollBar(JScrollBar.HORIZONTAL, 0, 1, 0, 8000);
        hbar.addAdjustmentListener(new MyAdjustmentListener());
        
        add(hbar, BorderLayout.CENTER);
        
        JButton zoomIn = new JButton("+");
        zoomIn.addActionListener(new MyButtonListener1());
        
        add(zoomIn,BorderLayout.WEST);
        
        JButton zoomOut = new JButton("-");
        zoomOut.addActionListener(new MyButtonListener2());
        
        add(zoomOut,BorderLayout.EAST);
        
        setupScrollBar();
        
//		JScrollPane sp = new JScrollPane(chartPanel);
//        setContentPane(sp);
//        setSize(400, 200);
	}
	
	public void setXAxisRange(double from, double to){
		chart.getXYPlot().getDomainAxis().setRange(from,to);
		this.pack();
		
	};
	
    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createXYLineChart(
            "SBD - Shot Boundary Detection", 
            "Frame", 
            "Value",
            dataset, 
            PlotOrientation.VERTICAL,
            true, 
            true, 
            false
        );
        final XYPlot plot = result.getXYPlot();


        plot.getDomainAxis().setRange(0,this.totalFrames);
        plot.getRangeAxis().setRange(0,maxY);
        return result;
    }
    
    public int createNewSeries(String title, Color c){
    	this.series.add(new XYSeries(title));
    	((XYSeriesCollection)this.dataset).addSeries(series.get(series.size()-1));
        int index = this.series.size()-1;
        chart.getXYPlot().getRenderer().setSeriesPaint(index, c);
    	return index;
    } 
    
    public void addValuesToSeries(double x, double y, int seriesIndex){
        if(Math.abs(y)>maxY){
            maxY = Math.abs(y);
            chart.getXYPlot().getRangeAxis().setRange(-maxY,maxY);
        }
    	this.series.get(seriesIndex).add(x, y);
    	this.pack();
    }

    public void addHorizontalLine(double y, Color c){
        ValueMarker marker = new ValueMarker(y);
        marker.setPaint(c);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.addRangeMarker(marker);
    }
    public void addVerticalLine(double y, Color c){
        ValueMarker marker = new ValueMarker(y);
        marker.setPaint(c);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.addDomainMarker(marker);
    }

    public double getMaxY() {
        return maxY;
    }

    class MyAdjustmentListener implements AdjustmentListener {
        public void adjustmentValueChanged(AdjustmentEvent e) {
        	setXAxisRange(hbar.getValue(),hbar.getValue()+range);
        	repaint();
        }
    }
    
    class MyButtonListener1 implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			setZoom(getZoom()/2);
			setupScrollBar();
			repaint();
		}
    	
    }
    
    class MyButtonListener2 implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			setZoom(getZoom()*2);
			setupScrollBar();
			repaint();
		}
    	
    }

}
