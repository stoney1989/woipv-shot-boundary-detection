package ch.fhnw.woipv.sbd.Loader;

import org.opencv.core.Mat;
import org.opencv.highgui.VideoCapture;

import java.io.IOException;

/**
 * Created by Administrator on 23.04.2015.
 */
public class VideoLoader {

    static{
        System.loadLibrary("opencv_java2411");
    }

    private VideoCapture vc = null;
    private int currentPosition;
    private String path = null;

    public VideoLoader( String path ){
        this.path = path;
    }

    public int getCurrentPosition(){
        return currentPosition;
    }

    public int getVideoSize()  {
        //System.out.println((int)vc.get(7));
        if(vc == null){
            try {
                reOpenVideo();
            } catch (IOException e) {
                e.printStackTrace();
                return 0;
            }
        }
        return (int)vc.get(7);
    }

    public int getFramesPerSecond(){
        if(vc == null){
            try {
                reOpenVideo();
            } catch (IOException e) {
                e.printStackTrace();
                return 0;
            }
        }
        return (int)vc.get(5);
    }

    public VideoLoader reOpenVideo() throws IOException {
        vc  = new VideoCapture();
        if( !(vc.open(path) ) ) throw new IOException("Could not open path: '" + path + "'!");
        currentPosition = -1;
        return this;
    }

    public VideoLoader windToNextFrame() throws IndexOutOfBoundsException, IOException {
        if( vc == null ) reOpenVideo();
        if( !vc.grab() || currentPosition + 1 >= getVideoSize() ) throw new IndexOutOfBoundsException("frame#: "+ getCurrentPosition() +" size:"+getVideoSize() );
        currentPosition++;
        return this;
    }

    public Mat extractFrameMatrix(){
        Mat frame = new Mat();
        vc.retrieve(frame);
        return frame;
    }


    public VideoLoader windToPosition( int position ) throws IOException {
        if( position < 0 )throw new IOException("position smaller than 0");
        if( position < currentPosition)reOpenVideo();
        while( currentPosition < position ) windToNextFrame();
        return this;
    }

}
