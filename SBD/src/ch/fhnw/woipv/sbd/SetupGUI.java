package ch.fhnw.woipv.sbd;

import ch.fhnw.woipv.sbd.Features.AbstractFeature;
import com.sun.org.apache.xalan.internal.utils.FeatureManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Nico Hartmann on 11.05.2015.
 */
public class SetupGUI {
    private JPanel panel;
    private JList list1;
    private JList list2;
    private JButton button1;

    static{
        System.loadLibrary("opencv_java2411");
    }


    public SetupGUI(final JFrame parent) {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(list1.getSelectedValue() != null && list2.getSelectedValue() != null){
                    parent.setVisible(false);
                    parent.dispose();
                    ShotBoundaryDetector sbd = new ShotBoundaryDetector(list2.getSelectedValue().toString(), list1.getSelectedValue().toString());

                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SetupGUI");
        SetupGUI s = new SetupGUI(frame);
        frame.setContentPane(s.panel);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);



//        DefaultListModel m1 = (DefaultListModel) (s.list1.getModel());
//        m1.addElement("asdf");

        frame.pack();



    }


    private void createUIComponents() {
        DefaultListModel m1 = new DefaultListModel();
        list1 = new JList(m1);
        File folder = new File("./videos");
        for (final File fileEntry : folder.listFiles()) {
            if(!fileEntry.isDirectory()) {
                m1.addElement(fileEntry.getName());
            }
        }

        DefaultListModel m2 = new DefaultListModel();
        list2 = new JList(m2);
        m2.addElement("HistogramFeature");
        m2.addElement("EdgeFeature");
    }
}
