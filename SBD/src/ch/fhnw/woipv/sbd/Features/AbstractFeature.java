package ch.fhnw.woipv.sbd.Features;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nico Hartmann on 29.04.2015.
 */
public abstract class AbstractFeature implements IFeature {

    private final double[][] data;
    protected final static ExecutorService executor = Executors.newCachedThreadPool();//.newFixedThreadPool(4);
    public AbstractFeature(int dimensions, int videoSize){
        this.data = new double[dimensions][videoSize];
    }

    public static void awaitShutDown(){
        executor.shutdown();
        try {
            executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public double[][] getData(){
        return data;
    }
}
