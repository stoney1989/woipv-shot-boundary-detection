package ch.fhnw.woipv.sbd.Features;

import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;

/**
 * Created by Nico Hartmann on 29.04.2015.
 */
public class HistogramFeatureWorker implements Runnable {



    private final static int BIN_SIZE = 16;

//    private final static MatOfInt CHANNELS =  new MatOfInt(0,1);
//    private final static MatOfFloat RANGES = new MatOfFloat(0,180, 0,256);
//    private final static MatOfInt BINS = new MatOfInt(BIN_SIZE,BIN_SIZE);

    private final static MatOfInt CHANNELS = new MatOfInt(0,1,2);
    //private final static MatOfFloat RANGES =  new MatOfFloat(0,180, 0,256, 0,256);
    private final static MatOfFloat RANGES =  new MatOfFloat(0,256, 0,256, 0,256);
    private final static MatOfInt BINS = new MatOfInt(BIN_SIZE,BIN_SIZE,BIN_SIZE);

    private final static Mat MASK = new Mat();

    private final static int BLUR_SIZE = 50;

    private static double calcHistogramDifference( Mat histogramA, Mat histogramB){
        double sum = 0.0;
        double difference = 0.0;
        for(int j = 0; j < histogramA.rows(); j++){
            for(int i = 0; i < histogramA.cols(); i++){
                difference = Math.abs(histogramA.get(j, i)[0] - histogramB.get(j, i)[0]);
                sum += difference * difference;
            }
        }

        return Math.sqrt(sum)/(histogramA.rows()*histogramA.cols());
    }

    private static Mat getHistogram( Mat frame){
        Imgproc.blur(frame, frame, new Size(BLUR_SIZE,BLUR_SIZE));
        //Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2HSV);

        Mat histogram = new Mat();
        Imgproc.calcHist(Arrays.asList(frame), CHANNELS, MASK, histogram, BINS, RANGES );
        return histogram;
    }

    private final VideoChunk chunk;
    private final double[][] featureData;

    public HistogramFeatureWorker(VideoChunk chunk, double[][] featureData){
        this.chunk = chunk;
        this.featureData = featureData;
    }

    @Override
    public void run() {
        Mat previousHistogram = null;
        for( int i = 0; i < chunk.getSize(); i++) {
            Mat frame = chunk.getFrame(i);
            Mat histogram = getHistogram(frame);
            frame.release();

            if (previousHistogram != null) {
                int index   = chunk.getFrom() + i;
                double old  = featureData[HistogramFeature.DIM_HISTORY][index];
                double diff = Imgproc.compareHist(histogram, previousHistogram, Imgproc.CV_COMP_BHATTACHARYYA);
                //double diff = Imgproc.compareHist(histogram, previousHistogram, Imgproc.CV_COMP_CHISQR);
                //double diff = calcHistogramDifference(histogram, previousHistogram);
                if(diff > old) featureData[HistogramFeature.DIM_HISTORY][index] = diff;
                previousHistogram.release();
            }
            previousHistogram = histogram;
        }
        if(previousHistogram!=null)previousHistogram.release();
        chunk.releaseFrameBuffer();
    }
}
