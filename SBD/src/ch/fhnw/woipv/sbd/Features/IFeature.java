package ch.fhnw.woipv.sbd.Features;

/**
 * Created by Nico Hartmann on 29.04.2015.
 */
public interface IFeature {
    double[][] getData();
    void feedChunk(VideoChunk chunk );
}
