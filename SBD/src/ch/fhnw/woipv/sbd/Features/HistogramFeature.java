package ch.fhnw.woipv.sbd.Features;

/**
 * Created by Nico Hartmann on 29.04.2015.
 */
public class HistogramFeature extends AbstractFeature {

    public final static int DIMENSIONS = 1;
    public final static int DIM_HISTORY = 0;

    public HistogramFeature(Integer videoSize) {
        super(DIMENSIONS, videoSize);
    }



    @Override
    public void feedChunk( VideoChunk chunk) {
        executor.submit( new HistogramFeatureWorker(chunk,getData()) );
    }
}
