package ch.fhnw.woipv.sbd.Features;

import org.opencv.core.Mat;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Administrator on 23.04.2015.
 */
public class VideoChunk {

    private final int size;
    private final int from;
    private final int to;
    private final Mat[] frameBuffer;

    private final AtomicInteger inUse;

    public VideoChunk( Mat[] buffer, int from , int uses ){

        //calculate real size
        int s = buffer.length;
        while( s > 0 && buffer[ s - 1 ] == null )s-=1;
        size = s;
        this.frameBuffer = buffer;

        this.from = from;
        this.to = (from + size) - 1;
        inUse = new AtomicInteger(uses);

    }




    public Mat getFrame( int index ){
        if( frameBuffer[index] != null ) return frameBuffer[index].clone();
        return null;
    }

    public void releaseFrameBuffer(){
        int uses = inUse.decrementAndGet();
        if( uses <= 0  ){
            for(int i = 0; i < size; i++){
                if( frameBuffer[i] != null ){
                    frameBuffer[i].release();
                    frameBuffer[i] = null;
                }
            }
        }
    }

    public int getSize(){
        return size;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }
}
