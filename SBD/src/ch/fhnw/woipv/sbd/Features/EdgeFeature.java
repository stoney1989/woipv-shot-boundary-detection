package ch.fhnw.woipv.sbd.Features;

/**
 * Created by Nico Hartmann on 29.04.2015.
 */
public class EdgeFeature extends AbstractFeature {

    public final static int DIMENSIONS  = 3;
    public final static int DIM_ECR     = 0;
    public final static int DIM_ECR_IN  = 1;
    public final static int DIM_ECR_OUT = 2;

    public EdgeFeature(Integer videoSize) {
        super(DIMENSIONS, videoSize);
    }



    @Override
    public void feedChunk( VideoChunk chunk) {
        executor.submit( new EdgeFeatureWorker(chunk,getData()) );
    }
}
