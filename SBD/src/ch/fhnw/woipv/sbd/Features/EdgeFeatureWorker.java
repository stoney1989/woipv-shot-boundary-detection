package ch.fhnw.woipv.sbd.Features;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Nico Hartmann on 29.04.2015.
 */
public class EdgeFeatureWorker implements Runnable {

    private final static int DILATION_SIZE = 10;
    private final static int BLUR_SIZE     = 10;


    private final static int CANNY_LOWER_THRESHOLD = 20;//20;
    private final static int CANNY_RATIO = 3;
    private final static int CANNY_UPPER_THRESHOLD = CANNY_LOWER_THRESHOLD * CANNY_RATIO;



    private final VideoChunk chunk;
    private final double[][] featureData;

    public EdgeFeatureWorker(VideoChunk chunk, double[][] featureData){
        this.chunk = chunk;
        this.featureData = featureData;
    }

    @Override
    public void run() {
        double ecr = 0;
        double ecr_in  = 0;
        double ecr_out = 0;

        Mat previousEdgeFrame = null;
        Mat previousInvertFrame = null;
        //s_n-1
        int previousEdgePixels = 0;

        for( int i = 0; i < chunk.getSize(); i++) {
            Mat frame = chunk.getFrame(i);

            int rows = frame.rows();
            int cols = frame.cols();
            int type = frame.type();
            Mat grayFrame   = new Mat( rows, cols, type );
            Mat edgeFrame   = new Mat( rows, cols, type );
            Mat invertFrame = new Mat( rows, cols, type );

            //transform to grayscale
            Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
            frame.release();

            //use canny edge detection:
            //reduce noise
            Imgproc.blur(grayFrame, grayFrame, new Size(BLUR_SIZE, BLUR_SIZE));
            Imgproc.Canny(grayFrame, edgeFrame, CANNY_LOWER_THRESHOLD, CANNY_UPPER_THRESHOLD, 3, true);
            grayFrame.release();

            //count edge pixels (s_n)
            int edgePixels = Core.countNonZero(edgeFrame);

            //dilate + invert edge frame
            Imgproc.dilate(edgeFrame, invertFrame, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(DILATION_SIZE,DILATION_SIZE)));
            Core.bitwise_not(invertFrame, invertFrame);

            if( previousEdgeFrame != null && previousInvertFrame != null){
                Mat ecFrame = new Mat( rows, cols, type );

                Core.bitwise_and(previousInvertFrame,edgeFrame,ecFrame);
                double ec_in = Core.countNonZero(ecFrame);

                //im.showImage(ecFrame);

                Core.bitwise_and(invertFrame, previousEdgeFrame,ecFrame);
                double ec_out = Core.countNonZero(ecFrame);

                //calculate Edge Change Ratio
                ecr_in  = ec_in  / edgePixels;
                ecr_out = ec_out / previousEdgePixels;

                ecr = Math.max(ecr_in, ecr_out);

                //clean up
                previousEdgeFrame.release();
                previousInvertFrame.release();
                ecFrame.release();
            }
            previousEdgeFrame   = edgeFrame;
            previousInvertFrame = invertFrame;
            previousEdgePixels  = edgePixels;

            int index   = chunk.getFrom() + i;

            double old  = featureData[EdgeFeature.DIM_ECR][index];
            if(ecr > old) featureData[EdgeFeature.DIM_ECR][index] = ecr;

            old  = featureData[EdgeFeature.DIM_ECR_IN][index];
            if(ecr_in > old) featureData[EdgeFeature.DIM_ECR_IN][index] = ecr_in;

            old  = featureData[EdgeFeature.DIM_ECR_OUT][index];
            if(ecr_out > old) featureData[EdgeFeature.DIM_ECR_OUT][index] = ecr_out;

        }
        chunk.releaseFrameBuffer();
    }
}
